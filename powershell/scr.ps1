param(
    [parameter(Mandatory=$true)] [string]$path
)


$projects = New-Object "System.Collections.Generic.List[string]"
 
$files = [System.IO.Directory]::GetFiles($path, "*.csproj", [System.IO.SearchOption]::AllDirectories)
 
for ($i=0; $i -lt $files.Length; $i++) {
    $file = $files[$i]
    if($file.Contains("iSynd.Common.Threading.Tasks")) {
        #continue 
    }

    $s = [System.IO.File]::ReadAllText($file);
    if(!$s.Contains("System.Core")) {
        continue
    }

    $name = [System.IO.Path]::GetFileName($file);
    $projects.Add($name)
}

if ($projects.Count -ne 0) {
    $message = ([System.String]::Join(", ", $projects))
    $message = "Next projects have invalid reference to Microsoft.Threading.Tasks library: " + $message
    throw $message
}
